from django.shortcuts import render
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin

from tutorial.models import TutorialPost

class HomeView(LoginRequiredMixin, TemplateView):
    template_name = "dashboard/pages/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["tutorials"] = TutorialPost.objects.all()
        return context
    
