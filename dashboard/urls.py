from django.urls import path, re_path, include
from . import views

from ticket.views import TicketCreateView
from ticket.views import TicketDetailView

app_name = 'dashboard'
urlpatterns = [
    re_path(r'^$', views.HomeView.as_view(), name='home'),
    re_path(r'^ticket/$', TicketCreateView.as_view(), name='ticket-create'),
    re_path(r'^ticket/(?P<sku>\w+)/$', TicketDetailView.as_view(), name='ticket-detail'),
]
