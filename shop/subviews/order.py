from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.utils.translation import ugettext_lazy as _
from django.views import View
from django.views.generic import ListView
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from zeep import Client

from cart.cart import Cart
from cart.forms import CartAddProductForm
from shop.models import Product
from shop.models import ProductCategory
from shop.models import ProductTag
from shop.serializer import OrderListSerializer
from shop.submodels.order import Order
from shop.submodels.order_item import OrderItem


class ProductsListView(ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'pages/shop/products.html'
    page_name = _(' محصولات')

    def get_context_data(self, **kwargs):
        context = super(ProductsListView, self).get_context_data(**kwargs)
        context['products'] = Product.objects.filter(important=True)
        context['recent_products'] = Product.objects.filter(important=True).order_by('-created')[:5]
        context['tags'] = ProductTag.objects.all()
        context['categories'] = ProductCategory.objects.all()

        return context


class OrderCheckoutView(View):
    template_name = 'pages/shop/checkout.html'

    view = {
        'page_name': 'Checkout'
    }

    def get(self, request, *args, **kwargs):
        cart = Cart(request)

        for item in cart:
            item['update_product_count_form'] = CartAddProductForm(
                initial={'quantity': item['quantity'], 'update': True})

        return render(request, self.template_name, {'view': self.view, 'cart': cart})


class OrderCheckoutApiView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, format=None):
        user_basket = Order.get_user_basket(request.user)
        MERCHANT = 'd3438f56-36bf-11e8-a385-005056a205be'
        client = Client('https://www.zarinpal.com/pg/services/WebGate/wsdl')
        amount = user_basket.get_order_price()
        description = "order with id %s and order_item_count:%s " % (user_basket.id, user_basket.items.count())
        CallbackURL = 'http://localhost:8001/shop/order/verify/'
        mobile_number = '989148480912'
        result = client.service.PaymentRequest(MERCHANT, amount, description, request.user.email, mobile_number,
                                               CallbackURL)
        if result.Status == 100:
            return redirect('https://www.zarinpal.com/pg/StartPay/' + str(result.Authority))
        else:
            return Response(_({'error': str(result.Status)}))


class OrderVerifyApiView(APIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        client = Client('https://www.zarinpal.com/pg/services/WebGate/wsdl')
        user_basket = Order.get_user_basket(request.user)
        amount = user_basket.get_order_price()
        MERCHANT = 'd3438f56-36bf-11e8-a385-005056a205be'
        if request.GET.get('Status') == 'OK':
            result = client.service.PaymentVerification(MERCHANT, request.GET['Authority'], amount)
            if result.Status == 100:
                return HttpResponse('Transaction success.\nRefID: ' + str(result.RefID))
            elif result.Status == 101:
                return HttpResponse('Transaction submitted : ' + str(result.Status))
            else:
                return HttpResponse('Transaction failed.\nStatus: ' + str(result.Status))
        else:
            return HttpResponse('Transaction failed or canceled by user')


class OrderViewSet(viewsets.ModelViewSet):
    serializer_class = OrderListSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return Order.get_user_basket(self.request.user)

    def list(self, request, *args, **kwargs):
        basket = Order.get_user_basket(request.user)
        serializer = OrderListSerializer(basket)
        return Response(serializer.data)


class OrderCreateApiView(View):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwargs):
        cart = Cart(request)

        if cart.is_empty():
            messages.warning(request, _('your cart is empty'))
            return HttpResponseRedirect(redirect_to="/shop/checkout/")

        else:
            user_basket = Order.get_user_basket(request.user)
            user_basket.items.all().delete()
            for item in cart:
                OrderItem.objects.create(order=user_basket, product=item['product'],
                                         quantity=item.get('quantity'))
            MERCHANT = 'd3438f56-36bf-11e8-a385-005056a205be'
            client = Client('https://www.zarinpal.com/pg/services/WebGate/wsdl')
            amount = user_basket.get_order_price()
            description = "order with id %s and order_item_count:%s " % (user_basket.id, user_basket.items.count())
            CallbackURL = 'http://localhost:8000/shop/verify/'
            mobile_number = '989148480912'
            result = client.service.PaymentRequest(MERCHANT, amount, description, request.user.email,
                                                   mobile_number,
                                                   CallbackURL)
            if result.Status == 100:
                return redirect('https://www.zarinpal.com/pg/StartPay/' + str(result.Authority))
            else:
                return HttpResponse('Error code: ' + str(result.Status))
