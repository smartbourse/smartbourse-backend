from rest_framework import viewsets, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from shop.models import OrderItem
from shop.serializer import OrderItemsSerializer
from shop.submodels.order import Order
from shop.subserializers.order import OrderListSerializer


class OrderItemsViewSet(viewsets.ModelViewSet):
    serializer_class = OrderItemsSerializer
    permission_classes = [IsAuthenticated]
    lookup_field = 'slug'

    def get_queryset(self):
        return OrderItem.objects.filter(order__customer=self.request.user).all()

    def create(self, request, *args, **kwargs):
        serializer = OrderItemsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save(user=request.user)
        order = Order.get_user_basket(request.user)
        serializer = OrderListSerializer(instance=order)
        return Response(serializer.data)

    def update(self, request, slug=None, *args, **kwargs):
        serializer = OrderItemsSerializer(data=request.data)
        instance = self.get_object()
        serializer.is_valid(raise_exception=True)
        serializer.update(instance, request.data)
        order = Order.get_user_basket(request.user)
        serializer = OrderListSerializer(instance=order)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        basket = Order.get_user_basket(request.user)
        order_items = self.get_object()
        if basket.items.filter(id=order_items.id):
            order_items.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)
