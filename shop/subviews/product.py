from django.utils.translation import ugettext_lazy as _
from django.views.generic import DetailView
from django.views.generic import ListView
from rest_framework import mixins, viewsets
from rest_framework.permissions import AllowAny

from cart.forms import CartAddProductForm
from shop.models import Product
from shop.models import ProductCategory
from shop.models import ProductTag
from shop.subserializers.product import ProductListSerializer


class ProductsListView(ListView):
    model = Product
    context_object_name = 'products'
    template_name = 'pages/shop/products.html'
    page_name = _(' محصولات')

    def get_context_data(self, **kwargs):
        context = super(ProductsListView, self).get_context_data(**kwargs)
        context['products'] = Product.objects.filter(available=True)
        context['recent_products'] = Product.objects.filter(available=True).order_by('-created')[:5]
        context['tags'] = ProductTag.objects.all()
        context['categories'] = ProductCategory.objects.all()

        return context


class ProductView(DetailView):
    template_name = 'pages/shop/product.html'
    page_name = _(' محصول')
    model = Product
    context_object_name = 'product'
    slug_field = 'slug'

    def get_context_data(self, **kwargs):
        context = super(ProductView, self).get_context_data(**kwargs)
        context['cart_add_product_form'] = CartAddProductForm()
        context['posts'] = Product.objects.all()
        context['recent_posts'] = Product.objects.order_by('-created')[:5]
        context['tags'] = ProductTag.objects.all()
        context['categories'] = ProductCategory.objects.all()

        return context


class ProductViewSet(mixins.ListModelMixin,
                     mixins.RetrieveModelMixin,
                     viewsets.GenericViewSet):
    permission_classes = [AllowAny]
    queryset = Product.objects.all()
    serializer_class = ProductListSerializer
    lookup_field = 'slug'


class ProductCategoryPostsView(DetailView):
    template_name = 'pages/blog/category.html'
    page_name = _('محصولات')
    slug_field = 'slug'
    context_object_name = 'category'
    model = ProductCategory

    def get_context_data(self, **kwargs):
        context = super(ProductCategoryPostsView, self).get_context_data(**kwargs)
        context['categories'] = ProductCategory.objects.all()
        context['recent_posts'] = Product.objects.order_by('-created')[:5]
        return context
