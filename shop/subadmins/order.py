from django.contrib import admin

from shop.submodels.order import Order
from shop.submodels.order_item import OrderItem


class OrderItemInline(admin.TabularInline):
    fields = ['product', 'quantity', 'slug']
    model = OrderItem
    extra = 1
    readonly_fields = ['slug']


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'customer', 'paid_status', 'created_time', 'updated_time']
    fields = ['customer', 'paid_status', 'invoice_number', 'created_time', 'updated_time']
    search_fields = ['customer']
    list_filter = ['paid_status']
    inlines = (OrderItemInline,)
    readonly_fields = ['paid_status',  'invoice_number', 'created_time', 'updated_time']


admin.site.register(OrderItem)
