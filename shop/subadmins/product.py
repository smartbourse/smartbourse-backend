from django.contrib import admin

from shop.submodels.product import Product
from shop.submodels.product import ProductBadge
from shop.submodels.product import ProductCategory
from shop.submodels.product import ProductLevel
from shop.submodels.product import ProductMovie
from shop.submodels.product import ProductPicture
from shop.submodels.product import ProductReview
from shop.submodels.product import ProductStatus
from shop.submodels.product import ProductTag


# Register your models here.


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'price', 'duration', 'available', 'teacher', 'category', 'badge', 'level', 'status',
        'created', 'updated')
    list_filter = ('available', 'teacher', 'category', 'badge', 'level', 'status')
    list_editable = ('category', 'available', 'status')
    filter_horizontal = ('pictures', 'tags')
    search_fields = ('title',)


@admin.register(ProductPicture)
class ProductPictureAdmin(admin.ModelAdmin):
    list_display = ('title', 'height_field', 'width_field', 'standard_size')
    list_filter = ('standard_size',)
    search_fields = ('title',)


@admin.register(ProductMovie)
class ProductMovieAdmin(admin.ModelAdmin):
    list_display = ('title', 'duration')
    search_fields = ('title',)


@admin.register(ProductCategory)
class ProductCategoryAdmin(admin.ModelAdmin):
    list_display = ('title', 'created', 'modified')
    search_fields = ('title',)


@admin.register(ProductTag)
class ProductTagAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)


@admin.register(ProductBadge)
class ProductBadgeAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)


@admin.register(ProductStatus)
class ProductStatusAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)


@admin.register(ProductLevel)
class ProductLevelAdmin(admin.ModelAdmin):
    list_display = ('title',)
    search_fields = ('title',)


@admin.register(ProductReview)
class ProductReviewAdmin(admin.ModelAdmin):
    list_display = ('post', 'by', 'created', 'modified')
    search_fields = ('title',)
