from django.urls import re_path
from rest_framework.routers import DefaultRouter

from . import views
from .views import OrderItemsViewSet
from .views import OrderViewSet
from .views import ProductViewSet

router = DefaultRouter()
router.register(r'shop/products', ProductViewSet, basename='products')
order_item_detail = OrderItemsViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'delete': 'destroy'
})

urlpatterns = [
    re_path(r'shop/order/$', OrderViewSet.as_view({'get': 'list', }), name='order_list'),
    re_path(r'shop/order/items/$', OrderItemsViewSet.as_view({'post': 'create'}), name='order_create'),
    re_path(r'shop/order/items/(?P<slug>[-\w]+)/$', order_item_detail, name='items'),
    re_path(r'^shop/order/checkout/$', views.OrderCheckoutApiView.as_view(), name='checkout'),
    re_path(r'^shop/order/verify/', views.OrderVerifyApiView.as_view(), name='verify'),
]
urlpatterns += router.urls
