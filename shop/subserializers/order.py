from rest_framework import serializers

from shop.models import Order
from shop.models import OrderItem
from shop.subserializers.product import ProductOrderSerializer


class OrderItemsSerializer(serializers.ModelSerializer):
    product = ProductOrderSerializer()
    item_price = serializers.SerializerMethodField()

    def get_item_price(self, items):
        return items.get_order_item_price()

    class Meta:
        model = OrderItem
        fields = ('product', 'quantity', 'item_price', 'slug')
        read_only_fields = ('slug',)


class OrderListSerializer(serializers.ModelSerializer):
    total_price = serializers.SerializerMethodField()
    items = OrderItemsSerializer(many=True)
    invoice_number = serializers.SerializerMethodField()

    class Meta:
        model = Order
        fields = ('created_time', 'total_price', 'active', 'items',)
        read_only_fields = ('user', 'total_price', 'created_time',)

    def get_total_price(self, order):
        return order.get_order_price()

    def get_invoice_number(self, order):
        return order.invoice_number


class OrderSerializer(serializers.ModelSerializer):
    total_price = serializers.SerializerMethodField()

    class Meta:
        model = Order
        lookup_field = 'id_order'
        fields = ('id', 'created_time', 'total_price', 'active', 'items',)
        read_only_fields = ('user', 'total_price', 'id', 'created_time',)

    def get_total_price(self, order):
        return order.get_order_price()
