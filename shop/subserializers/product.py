from rest_framework import serializers

from shop.models import Product


class ProductListSerializer(serializers.ModelSerializer):
    teacher_name = serializers.SerializerMethodField()
    status_title = serializers.SerializerMethodField()
    level_title = serializers.SerializerMethodField()
    tags_title = serializers.SerializerMethodField()
    pictures = serializers.SerializerMethodField()
    category_title = serializers.SerializerMethodField()
    badge_title = serializers.SerializerMethodField()

    def get_teacher_name(self, product):
        teacher = product.teacher
        return '%s %s' % (teacher.first_name, teacher.last_name)

    def get_status_title(self, product):
        return product.status.title

    def get_level_title(self, product):
        return product.level.title

    def get_tags_title(self, product):
        return product.tags.values_list('title')

    def get_pictures(self, product):
        result = []
        for pic in product.pictures.all():
            result.append(self.context['request'].build_absolute_uri(pic.picture.url))
        return result

    def get_category_title(self, product):
        return product.category.title

    def get_badge_title(self, product):
        return product.badge.title

    class Meta:
        model = Product
        lookup_field = 'slug'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }

        fields = (
            'title', 'slug', 'summary', 'content', 'price', 'duration', 'available', 'preview', 'teacher_name',
            'category_title',
            'badge_title',
            'level_title', 'status_title', 'tags_title', 'pictures', 'created', 'updated')
        read_only_fields = (
            'title', 'slug', 'summary', 'content', 'price', 'duration', 'available', 'preview', 'teacher',
            'category',
            'badge',
            'level', 'status', 'tags', 'pictures', 'created', 'updated')


class ProductOrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product

        fields = ('title', 'summary', 'price',)
