from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from shop.models import Order
from shop.models import OrderItem
from shop.serializer import ProductOrderSerializer
from shop.submodels.product import Product


class OrderItemsSerializer(serializers.ModelSerializer):
    invoice_number = serializers.SerializerMethodField()
    product = ProductOrderSerializer(read_only=True, required=False)
    product_title = serializers.CharField(write_only=True, required=False)
    lookup_field = 'slug'
    extra_kwargs = {
        'url': {'lookup_field': 'slug'}
    }

    def get_invoice_number(self, order_item):
        return order_item.order.invoice_number

    class Meta:
        model = OrderItem
        fields = ('invoice_number', 'product', 'quantity', 'slug', 'product_title')
        read_only_fields = ('slug',)

    def create(self, data):
        user = data.pop('user')
        order = Order.get_user_basket(user)
        product = data.get('product_title')
        quantity = data.get('quantity')
        if not product:
            raise serializers.ValidationError(_('Send Product'))
        try:
            product = Product.objects.get(title=product)
        except Product.DoesNotExist:
            raise serializers.ValidationError(_('Invalid Product'))
        try:
            order_item = order.items.get(product=product)
        except OrderItem.DoesNotExist:
            if quantity:
                order_item = OrderItem.objects.create(product=product, quantity=quantity, order=order)
            else:
                order_item = OrderItem.objects.create(product=product, order=order)
        return order_item

    def update(self, instance, data):
        quantity = data.get('quantity')
        if quantity:
            instance.quantity = quantity
            instance.save()

        return instance
