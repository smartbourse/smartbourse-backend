from django.urls import re_path

from . import views
from .views import OrderCheckoutView

app_name = 'shop'

urlpatterns = [
    re_path(r'^$', views.ProductsListView.as_view(), name='products'),
    re_path(r'^checkout/$', OrderCheckoutView.as_view(), name='checkout'),
    re_path(r'^product/(?P<slug>[\w-]+)/$', views.ProductView.as_view(), name='product'),
    re_path(r'^order_create/$', views.OrderCreateApiView.as_view(), name='order_create'),
    re_path(r'^verify/', views.OrderVerifyApiView.as_view(), name='verify'),
    re_path(r'^categpry/(?P<slug>[\w\-]+)/posts/$', views.ProductCategoryPostsView.as_view(), name='category_posts'),
]
