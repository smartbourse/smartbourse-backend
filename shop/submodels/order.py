import uuid

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _

from utils.custom_models import TimeStampModel


class Order(TimeStampModel):
    customer = models.ForeignKey(User, related_name='orders', on_delete=models.CASCADE)
    invoice_number = models.UUIDField(_('Invoice Number'), unique=True, editable=False)
    # discount = models.PositiveIntegerField(_('Discount'), default=0)
    paid_status = models.BooleanField(_('Paid Status'), default=False, editable=False)

    class Meta:
        verbose_name = _('Order')
        verbose_name_plural = _('Orders')
        ordering = ('-created_time',)

    def __str__(self):
        return '%s-%s:%s' % (self.customer.first_name, self.customer.last_name, self.invoice_number)

    def get_order_price(self):
        return sum(item.get_order_item_price() for item in self.items.all())

    def save(self, *args, **kwarg):
        if not self.pk:
            self.invoice_number = uuid.uuid4()
        return super(Order, self).save(*args, **kwarg)


    @classmethod
    def get_user_basket(cls, user):
        last_order = cls.objects.filter(active=True, customer=user).last()
        if not last_order:
            last_order = cls.objects.create(customer=user)
        return last_order
