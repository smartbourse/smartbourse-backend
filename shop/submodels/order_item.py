from django.db import models
from django.utils.text import slugify
from django.utils.translation import ugettext_lazy as _

from utils.custom_models import TimeStampModel


class OrderItem(TimeStampModel):
    order = models.ForeignKey('Order', verbose_name=_('Order'), related_name='items', on_delete=models.CASCADE,
                              blank=True)
    product = models.ForeignKey('Product', verbose_name=_('Product'), related_name='order_items',
                                on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(_('Quantity'), default=1)
    slug = models.SlugField(_('Slug'), db_index=True, max_length=140, editable=False)

    class Meta:
        verbose_name = _('Order Item')
        verbose_name_plural = _('Orders Item')
        ordering = ('-created_time',)

    def __str__(self):
        return 'order:%s,product:%s' % (self.order_id, self.product_id)

    def get_order_item_price(self):
        return self.product.price * self.quantity

    def save(self, *args, **kwargs):
        self.slug = slugify(self.product.title, allow_unicode=True)
        super(OrderItem, self).save(*args, **kwargs)
