from django.urls import (
    path, 
    re_path, 
    include
)


from . import views


app_name = 'tutorial'
urlpatterns = [
    re_path(r'^$', views.TutorialView.as_view(), name='posts'),
    re_path(r'^categpry/(?P<slug>[\w\-]+)/posts/$', views.TutorialCategoryPostsView.as_view(), name='category_posts'),
    re_path(r'^search/$', views.TutorialSearchPostView.as_view(), name='search_posts'),
    re_path(r'^(?P<slug>[\w\-]+)/$', views.SingleTutorialView.as_view(), name='post'),
]
