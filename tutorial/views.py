from django.shortcuts import render
from django.views.generic import (
    TemplateView,
    ListView,
    DetailView,
    View,
)
from django.urls import reverse
from django.urls import reverse_lazy
from django.shortcuts import redirect
from django.contrib.postgres.search import SearchVector

from django.utils.translation import ugettext_lazy as _

from .models import TutorialPost
from .models import TutorialCategory
from .models import TutorialTag

class TutorialView(ListView):
    template_name = 'pages/tutorial/posts.html'
    page_name = _('آموزش')
    model = TutorialPost
    context_object_name = 'posts'

    def get_context_data(self, **kwargs):
        context = super(TutorialView, self).get_context_data(**kwargs)

        context['posts'] = TutorialPost.objects.all()
        context['recent_posts'] = TutorialPost.objects.order_by('-created')[:5]
        context['tags'] = TutorialTag.objects.all()
        context['categories'] = TutorialCategory.objects.all()

        return context


class TutorialSearchPostView(View):
    template_name = 'pages/tutorial/posts.html'
    context = dict()
    page_name = 'آموزش'

    def get(self, request, *args, **kwargs):
        return redirect(reverse_lazy('tutorial:posts'))

    def post(self, request, *args, **kwargs):
        query = self.request.POST.get('search_content')
        vector = SearchVector('content', 'title')
        self.context['view'] = { 'page_name': self.page_name }
        self.context['tags'] = TutorialTag.objects.all()
        self.context['categories'] = TutorialCategory.objects.all()
        self.context['recent_posts'] = TutorialPost.objects.order_by('-created')[:5]
        self.context['posts'] = TutorialPost.objects.annotate(search = vector).filter(search__icontains = query)
        return render(request, self.template_name, self.context)

class TutorialCategoryPostsView(DetailView):
    template_name = 'pages/tutorial/category.html'
    page_name = _('آموزش')
    slug_field = 'slug'
    context_object_name  = 'category'
    model = TutorialCategory

    def get_context_data(self, **kwargs):
        context = super(TutorialCategoryPostsView, self).get_context_data(**kwargs)
        context['categories'] = TutorialCategory.objects.all()
        context['recent_posts'] = TutorialPost.objects.order_by('-created')[:5]
        return context

class SingleTutorialView(DetailView):
    model = TutorialPost
    template_name = 'pages/tutorial/post.html'
    page_name = _('آموزش')
    slug_field = 'slug'
    context_object_name  = 'post'

    def get_context_data(self, **kwargs):
        context = super(SingleTutorialView, self).get_context_data(**kwargs)
        context['posts'] = TutorialPost.objects.all()
        context['recent_posts'] = TutorialPost.objects.order_by('-created')[:5]
        context['tags'] = TutorialTag.objects.all()
        context['categories'] = TutorialCategory.objects.all()
        return context
