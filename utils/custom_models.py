from django.db import models
from django.utils.translation import ugettext_lazy as _


class TimeStampModel(models.Model):
    created_time = models.DateTimeField(_('created time'), auto_now_add=True)
    updated_time = models.DateTimeField(_('updated time'), auto_now=True)
    active = models.BooleanField(_('active'), default=True)

    class Meta:
        verbose_name = _('Time Stamp')
        verbose_name_plural = _('Times Stamp')
        abstract = True
