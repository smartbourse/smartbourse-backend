from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.generic import View

from shop.models import Product
from .cart import Cart
from .forms import CartAddProductForm


class CartAddView(View):
    lookup_field = 'slug'

    def post(self, request, slug, *args, **kwargs):
        cart = Cart(request)
        product = get_object_or_404(Product, slug=slug)
        form = CartAddProductForm(request.POST)

        if form.is_valid():
            form_data = form.cleaned_data
            cart.add(product=product, quantity=form_data['quantity'], update_count=form_data['update'])

        return redirect('shop:checkout')


class CartRemoveView(View):

    def get(self, request, slug, *args, **kwargs):
        cart = Cart(request)
        product = get_object_or_404(Product, slug=slug)
        cart.remove(product)

        return redirect('shop:checkout')


class CartDetailView(View):
    def get(self, request, *args, **kwargs):
        cart = Cart(request)
        return render(request, 'pages/shop/checkout.html', {'cart': cart})
