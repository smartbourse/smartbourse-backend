from django.urls import path, re_path, include
from . import views

app_name = 'technofund'
urlpatterns = [
    re_path(r'^$', views.AnalyzeView.as_view(), name='posts'),
    re_path(r'^categpry/(?P<slug>[\w\-]+)/posts/$', views.TechnofundCategoryPostsView.as_view(), name='category_posts'),
    re_path(r'^search/$', views.TechnofundSearchPostView.as_view(), name='search_posts'),
    re_path(r'^(?P<slug>[\w\-]+)/$', views.SingleAnalyzeView.as_view(), name='post'),
]