from django.shortcuts import render

from django.views.generic import TemplateView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import View
from django.utils.translation import ugettext_lazy as _
from .models import AnalyzePost
from .models import AnalyzeCategory
from .models import AnalyzeTag

from django.contrib.postgres.search import SearchVector
from django.urls import reverse_lazy
from django.shortcuts import redirect

class AnalyzeView(ListView):
    template_name = 'pages/analysis/posts.html'
    page_name = _('تکنوفاند')
    context_object_name = "posts"
    model = AnalyzePost

    def get_context_data(self, **kwargs):
        context = super(AnalyzeView, self).get_context_data(**kwargs)
        context['recent_posts'] = AnalyzePost.objects.order_by('-created')[:5]
        context['tags'] = AnalyzeTag.objects.all()
        context['categories'] = AnalyzeCategory.objects.all()
        return context

class TechnofundSearchPostView(View):
    template_name = 'pages/analysis/posts.html'
    context = dict()
    page_name = 'تکنوفاند'

    def get(self, request, *args, **kwargs):
        return redirect(reverse_lazy('technofund:posts'))

    def post(self, request, *args, **kwargs):
        query = self.request.POST.get('search_content')
        vector = SearchVector('content', 'title')
        self.context['view'] = { 'page_name': self.page_name }
        self.context['tags'] = AnalyzeTag.objects.all()
        self.context['categories'] = AnalyzeCategory.objects.all()
        self.context['posts'] = AnalyzePost.objects.annotate(search = vector).filter(search__icontains = query)
        return render(request, self.template_name, self.context)

class TechnofundCategoryPostsView(DetailView):
    template_name = 'pages/analysis/category.html'
    page_name = _('تکنوفاند')
    slug_field = 'slug'
    context_object_name  = 'category'
    model = AnalyzeCategory

    def get_context_data(self, **kwargs):
        context = super(TechnofundCategoryPostsView, self).get_context_data(**kwargs)
        context['categories'] = AnalyzeCategory.objects.all()
        context['recent_posts'] = AnalyzePost.objects.order_by('-created')[:5]
        return context


class SingleAnalyzeView(DetailView):
    model = AnalyzePost
    template_name = 'pages/analysis/post.html'
    page_name = _('تکنوفاند')
    slug_field = 'slug'
    context_object_name  = 'post'

    def get_context_data(self, **kwargs):
        context = super(SingleAnalyzeView, self).get_context_data(**kwargs)
        context['posts'] = AnalyzePost.objects.all()
        context['recent_posts'] = AnalyzePost.objects.order_by('-created')[:5]
        context['tags'] = AnalyzeTag.objects.all()
        context['categories'] = AnalyzeCategory.objects.all()
        return context
