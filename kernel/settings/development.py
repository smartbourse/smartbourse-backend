from .base import *
from .secure import *
from .packages import *

DEBUG = True
ALLOWED_HOSTS = ['localhost','192.168.1.27']
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
