from django.urls import include, re_path
from rest_framework_simplejwt import views as jwt_views
from rest_framework_simplejwt.views import TokenVerifyView
from rest_framework_swagger.views import get_swagger_view

from api.views import LogoutAndBlacklistRefreshTokenForUserView

from rest_framework_jwt.views import verify_jwt_token
from rest_framework_jwt.views import obtain_jwt_token
from rest_framework_jwt.views import refresh_jwt_token

schema_view = get_swagger_view(title='Smart Bourse API Documentation')

urlpatterns = [
    re_path(r'^api-documentation/', schema_view),
    re_path(r'^api-auth/', include('rest_framework.urls')),
    re_path(r'^jwt/login/$', obtain_jwt_token),
    re_path(r'^jwt/token-refresh/$', refresh_jwt_token),
    re_path(r'^jwt/verify/$', verify_jwt_token),

    re_path(r'^token/$', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    re_path(r'^token/refresh/$', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
    re_path(r'^token/verify/$', TokenVerifyView.as_view(), name='token_verify'),
    re_path(r'^token/blacklist/$', LogoutAndBlacklistRefreshTokenForUserView.as_view(), name='token_black_list'),
    re_path(r'^v1/', include('api.v1.analysis.urls')),
    re_path(r'^v1/', include('api.v1.news.urls')),
    re_path(r'^v1/', include('api.v1.contact.urls')),
    re_path(r'^v1/', include('api.v1.primarymarket.urls')),
    re_path(r'^v1/', include('api.v1.openaccount.urls')),
    re_path(r'^v1/', include('api.v1.tutorial.urls')),
    re_path(r'^v1/', include('shop.api_urls')),
]

