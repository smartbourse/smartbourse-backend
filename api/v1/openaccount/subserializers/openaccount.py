from django.contrib.auth.models import User

from rest_framework import serializers

from pages.models import OpenAccount


class OpenAccountSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = OpenAccount
        fields = ('full_name', 'national_code', 'phone', 'investment', 'familiarity',)
