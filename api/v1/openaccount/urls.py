from rest_framework import routers
from django.urls import re_path

from . import views as view

router = routers.SimpleRouter()


router.register(r'openaccount', view.OpenAccountViewSet)

urlpatterns = router.urls
