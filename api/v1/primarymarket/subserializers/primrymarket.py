from django.contrib.auth.models import User

from rest_framework import serializers

from primarymarket.models import PrimaryMarket


class PrimaryMarketSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = PrimaryMarket
        fields = ('title', 'abbrivation', 'date', 'archive', 'content',)