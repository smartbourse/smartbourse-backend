from rest_framework import routers
from django.urls import re_path

from . import views as view

router = routers.SimpleRouter()


router.register(r'primarymarket', view.PrimaryMarketViewSet)

urlpatterns = router.urls
