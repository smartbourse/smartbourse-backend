from rest_framework import viewsets

from rest_framework.decorators import action

from rest_framework.response import Response

from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import AllowAny

from rest_framework.exceptions import MethodNotAllowed

from rest_framework.permissions import DjangoModelPermissions

from rest_framework.filters import SearchFilter

from rest_framework.filters import OrderingFilter

from django_filters.rest_framework import DjangoFilterBackend

from primarymarket.models import PrimaryMarket

from api.v1.primarymarket.subserializers.primrymarket import PrimaryMarketSerializer

class PrimaryMarketViewSet(viewsets.ModelViewSet):

    queryset = PrimaryMarket.objects.all()

    serializer_class = PrimaryMarketSerializer

    permission_classes = [
        AllowAny
    ]

    def post(self, request, *args, **kwargs):
        raise MethodNotAllowed("POST")
    
    def retrieve(self, request, pk=None):
        raise MethodNotAllowed("PUT")

    def retrieve(self, request, pk=None):
        raise MethodNotAllowed("PUT")

    def partial_update(self, request, pk=None):
        raise MethodNotAllowed("PUT")

    def destroy(self, request, pk=None):
        raise MethodNotAllowed("DELETE")