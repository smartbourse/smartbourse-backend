from django.contrib.auth.models import User

from rest_framework import serializers

from pages.models import Contact


class ContactSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = Contact
        fields = ('full_name', 'email', 'from_where', 'subject', 'message',)