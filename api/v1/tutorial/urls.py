from rest_framework import routers
from django.urls import re_path

from . import views as view

router = routers.SimpleRouter()

router.register(r'tutorial/category', view.TutorialCategoryViewSet)
router.register(r'tutorial', view.TutorialViewSet)
# router.register(r'analysis', view.AnalyzePostViewSet)

urlpatterns = router.urls
