from django.contrib.auth.models import User

from rest_framework import serializers

from tutorial.models import TutorialPost


class TutorialSerializer(serializers.ModelSerializer):


    class Meta:
        model = TutorialPost
        fields = ('title', 'slug', 'summary', 'content', 'important', 'pictures', 'is_shown', 'category', 'badge', 'level')
        depth = 1