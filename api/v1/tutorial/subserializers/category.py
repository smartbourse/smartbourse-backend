from django.contrib.auth.models import User

from rest_framework import serializers

from tutorial.models import TutorialPost


class CategorySerializer(serializers.ModelSerializer):

    class Meta:
        model = TutorialPost
        fields = ('title', 'slug',)
