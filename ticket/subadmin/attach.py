from django.contrib import admin

from  ticket.models import Attach
from .general import GeneralAdminDashboardDesign


@admin.register(Attach)
class AttachAdmin(GeneralAdminDashboardDesign):
     list_filter = ('created', 'ticket_id')
     list_display = ('ticket_id', 'comments_id')
     list_display_links = ('ticket_id', 'comments_id')

     fields = (
         ('ticket_id', 'comments_id'),
         ('uploaded_file', ),
     )
