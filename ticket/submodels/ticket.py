import secrets

from django.conf import settings
from django.db import models
from django.contrib.auth.models import User
from django.shortcuts import reverse

from .general import AnswerStatus, Identification, PriorityStatus, TimestampedModel
from .department import Department


class Ticket(AnswerStatus, Identification, PriorityStatus):
    department_id = models.ForeignKey(Department, related_name='tickets', on_delete = models.SET_NULL, null = True)

    raised_by = models.ForeignKey(User, related_name='tickets', on_delete=models.CASCADE, editable = False)
    
    subject = models.CharField(max_length = 128)
    content = AnswerStatus.message

    class Meta:
        ordering = ['-created']

    def save(self, *args, **kwargs):
       if not self.pk:
            self.sku = secrets.token_hex(16)
       super(Ticket, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('dashboard:ticket-create', kwargs={})

    def __str__(self):
        return self.sku
