from django.db import models
from django.conf import settings
from django.contrib.auth.models import User
from .general import TimestampedModel, Identification

import secrets


class Department(TimestampedModel, Identification):
    users = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name = "department_member")
    manager_id = models.OneToOneField(settings.AUTH_USER_MODEL, related_name = "users", on_delete = models.SET_NULL, blank = True, null = True) 

    name = models.CharField(max_length = 128)
    description = models.TextField()
    
    joined_date = TimestampedModel.created

    official = models.BooleanField(default = False)

    class Meta:
        ordering = ['name']

    def save(self, *args, **kwargs):
       if not self.pk:
            self.sku = secrets.token_hex(16)
       super(Department, self).save(*args, **kwargs)

    def __str__(self):
        return self.name
