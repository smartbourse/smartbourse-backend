from django.db import models

from .general import TimestampedModel
from .ticket import Ticket
from .comments import Comments


class Attach(TimestampedModel):
     id = models.AutoField(primary_key = True, auto_created = True)
     ticket_id = models.ForeignKey(Ticket, on_delete = models.CASCADE)
     comments_id = models.ForeignKey(Comments, related_name = 'attachs', on_delete = models.CASCADE, blank = True, null = True)

     uploaded_file = models.FileField(upload_to = 'upload/ticket', null = True, blank = True)


     def __str__(self):
         return "for {} ticket".format(self.ticket_id)
