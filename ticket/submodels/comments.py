from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
import secrets
from django.contrib.auth.models import User

from .general import Identification, AnswerStatus
# from .ticket import Ticket


class Comments(Identification, AnswerStatus):
    staff_id = models.ForeignKey(settings.AUTH_USER_MODEL, related_name = 'comments', on_delete = models.CASCADE)
    ticket_id = models.ForeignKey('Ticket', related_name='replies', on_delete = models.CASCADE)
    parent = models.ForeignKey("self", on_delete = models.SET_NULL, null = True, blank = True)
    commnet = AnswerStatus.message
    
    class Meta:
        ordering = ['created']
        verbose_name_plural = _('Comments')

    def save(self, *args, **kwargs):
        if not self.pk:
            self.sku = secrets.token_hex(16)
        super(Comments, self).save(*args, **kwargs)

    def __str__(self):
        return "by {} for {}".format(self.staff_id, self.ticket_id)
