from django.conf import settings
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.dispatch import receiver

from .models import Contact


@receiver(post_save, sender=Contact)
def email_send(sender, instance, created, **kwargs):
    if created:
        email_address = instance.email
        subject = 'تاییدیه دریافت ایمیل'
        text = '%s عزیز بزودی با شما تماس خواهیم گرفت.' % instance.full_name
        send_mail(subject,
                  text,
                  settings.EMAIL_HOST_USER,
                  [email_address, ],
                  fail_silently=False, )
