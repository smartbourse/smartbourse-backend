from django.core.mail import send_mail
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import CreateView
from django.views.generic import TemplateView

from analysis.models import AnalyzePost
from news.models import NewsCategory
from news.models import NewsPost
from .forms import ContactForm
from .forms import OpenAccountForm
from .models import Contact
from .models import OpenAccount


class HomeView(CreateView):
    template_name = 'pages/index.html'
    model = Contact
    form_class = ContactForm
    success_url = reverse_lazy('pages:home')
    page_name = _('خانه')

    # def form_valid(self, form):
    #     self.object = form.save()
    #     send_mail(
    #         'Smart Bourse Contact: {}'.format(form.data['subject']),
    #         '{}'.format(form.data['message']),
    #         'test@gmail.com',
    #         ['sa.goldeneagle@gmail.com'],
    #         fail_silently=False,
    #     )
    #     return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['latest_posts'] = AnalyzePost.objects.all()[:3]
        context['recent_news'] = NewsPost.objects.filter(important=True).filter(is_shown=True)[:5]
        context['technofund'] = AnalyzePost.objects.filter(important=True).filter(is_shown=True)[:5]
        context['news_categories'] = NewsCategory.objects.all()
        return context


class AboutView(TemplateView):
    template_name = "pages/about.html"


class ContactView(CreateView):
    template_name = 'pages/contact.html'
    model = Contact
    form_class = ContactForm
    page_name = _('تماس با ما')
    success_url = reverse_lazy('pages:contact')
    is_rtl = False

    def form_valid(self, form):
        self.object = form.save()
        send_mail(
            'Smart Bourse Contact: {}'.format(form.data['subject']),
            '{}'.format(form.data['message']),
            'animatedidea@gmail.com',
            ['sa.goldeneagle@gmail.com'],
            fail_silently=False,
        )
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(ContactView, self).get_context_data(**kwargs)

        return context


class OpenAccountView(CreateView):
    template_name = 'pages/open-account.html'
    model = OpenAccount
    form_class = OpenAccountForm
    page_name = _('افتتاح حساب آنلاین')
    success_url = reverse_lazy('pages:success')
    is_rtl = False

    def form_valid(self, form):
        # import pdb; pdb.set_trace();
        self.object = form.save()
        send_mail(
            'Smart Bourse Contact: {}'.format(form.data['full_name']),
            '{}'.format(form.data['phone']),
            'animatedidea@gmail.com',
            ['sa.goldeneagle@gmail.com'],
            fail_silently=False,
        )
        return HttpResponseRedirect(self.get_success_url())

    def get_context_data(self, **kwargs):
        context = super(OpenAccountView, self).get_context_data(**kwargs)

        return context


class SuccessView(TemplateView):
    template_name = "pages/success.html"
