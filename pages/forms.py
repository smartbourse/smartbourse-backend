from django import forms
from .models import Contact
from .models import OpenAccount


class ContactForm(forms.ModelForm):
    class Meta:
        model = Contact
        fields = [
            'full_name',
            'email',
            'from_where',
            'subject',
            'message',
        ]

        labels = {
            "full_name": 'نام خود را وارد کنید',
            "email": 'ایمیل خود را وارد کنید',
            "from_where": 'از کجا با ما آشنا شدید',
            "subject": 'موضوع خود را وارد کنید',
            "message": 'پیام خود را وارد کنید',
        }



class OpenAccountForm(forms.ModelForm):
    class Meta:
        model = OpenAccount
        fields = [
            'full_name',
            'national_code',
            'phone',
            'investment',
            'familiarity',
        ]

        labels = {
            "full_name": 'نام و نام خانوادگی',
            "national_code": 'کد ملی',
            "phone": 'شماره تماس',
            "investment": 'میزان سرمایه',
            "familiarity": 'میزان آشنایی',
        }

