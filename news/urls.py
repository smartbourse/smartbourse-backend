from django.urls import path, re_path, include
from . import views

app_name = 'news'
urlpatterns = [
    re_path(r'^$', views.NewsListView.as_view(), name='posts'),
    re_path(r'^category/(?P<slug>[\w\-]+)/posts/$', views.NewsCategoryPostsView.as_view(), name='category_posts'),
    re_path(r'^search/$', views.NewsSearchPostView.as_view(), name='search_posts'),
    re_path(r'^(?P<slug>[\w\-]+)/$', views.SinglePostNewsDetail.as_view(), name='post'),
]
