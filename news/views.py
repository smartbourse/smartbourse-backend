from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic import DetailView
from django.views.generic import ListView
from django.views.generic import View
from django.utils.translation import ugettext_lazy as _

from .models import NewsCategory
from .models import NewsPost
from .models import NewsTag

from django.contrib.postgres.search import SearchVector
from django.urls import reverse_lazy
from django.shortcuts import redirect

class NewsListView(ListView):
    model = NewsPost
    context_object_name = 'posts'
    template_name = 'pages/news/posts.html'
    page_name = _('اخبار')

    def get_context_data(self, **kwargs):
        context = super(NewsListView, self).get_context_data(**kwargs)
        
        context['posts'] = NewsPost.objects.filter(important = True)
        context['recent_posts'] = NewsPost.objects.filter(important = True).order_by('-created')[:5]
        context['tags'] = NewsTag.objects.all()
        context['categories'] = NewsCategory.objects.all()

        return context

class NewsSearchPostView(View):
    template_name = 'pages/news/posts.html'
    context = dict()
    page_name = 'اخبار'

    def get(self, request, *args, **kwargs):
        return redirect(reverse_lazy('news:posts'))

    def post(self, request, *args, **kwargs):
        query = self.request.POST.get('search_content')
        vector = SearchVector('content', 'title')
        self.context['view'] = { 'page_name': self.page_name }
        self.context['tags'] = NewsPost.objects.all()
        self.context['categories'] = NewsCategory.objects.all()
        self.context['recent_posts'] = NewsPost.objects.filter(important = True).order_by('-created')[:5]
        self.context['posts'] = NewsPost.objects.annotate(search = vector).filter(search__icontains = query)
        return render(request, self.template_name, self.context)

class NewsCategoryPostsView(DetailView):
    model = NewsCategory
    template_name = 'pages/news/category.html'
    page_name = _('اخبار')
    slug_field = 'slug'
    context_object_name  = 'category'

    def get_context_data(self, **kwargs):
        context = super(NewsCategoryPostsView, self).get_context_data(**kwargs)
        context['categories'] = NewsCategory.objects.all()
        context['recent_posts'] = NewsPost.objects.order_by('-created')[:5]
        return context

class SinglePostNewsDetail(DetailView):
    model = NewsPost
    template_name = 'pages/news/post.html'
    context_object_name = 'post'
    page_name = _('اخبار')

    def get_context_data(self, **kwargs):
        context = super(SinglePostNewsDetail, self).get_context_data(**kwargs)
        context['posts'] = NewsPost.objects.all()
        context['recent_posts'] = NewsPost.objects.order_by('-created')[:5]
        context['tags'] = NewsTag.objects.all()
        context['categories'] = NewsCategory.objects.all()
        return context